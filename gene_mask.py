import os
import glob
import matplotlib.pyplot as plt
import json
import numpy as np
import io
import base64
from PIL import Image
import cv2
import math
import numpy as np
from scipy import spatial
import string
import random
import traceback
import tqdm
import shutil

def img_b64_to_arr(img_b64):
    f = io.BytesIO()
    f.write(base64.b64decode(img_b64))
    img_arr = np.array(Image.open(f))
    return img_arr

def visualize(**images):
    """PLot images in one row."""
    n = len(images)
    plt.figure(figsize=(16, 5))
    for i, (name, image) in enumerate(images.items()):
        plt.subplot(1, n, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.title(' '.join(name.split('_')).title())
        plt.imshow(image)
    plt.show()



# fns = glob.glob('./2stage_mindata_xiliewen/*.json')
# fns = glob.glob('./2stage_mindata_xiliewen_zhang/train_demo/*.json')


SIZE_W, SIZE_H = 640, 320
basedir = '/mnt/nfs/public/xin-yuan/anno-data/20210106-0-0'
train_basedir = os.path.join(basedir, 'train')
test_basedir = os.path.join(basedir, 'test')

target_dir = '/mnt/nfs/public/xin-yuan/anno-data/20210106-0-0/mask/'
train_targetdir = os.path.join(target_dir, 'train_mask')
test_targetdir = os.path.join(target_dir, 'test_mask')

if not os.path.exists(target_dir):
    os.makedirs(target_dir)

if not os.path.exists(train_targetdir):
    os.makedirs(train_targetdir)
if not os.path.exists(test_targetdir):
    os.makedirs(test_targetdir)

if not os.path.exists(train_basedir):
    os.makedirs(train_basedir)
if not os.path.exists(test_basedir):
    os.makedirs(test_basedir)

fns = glob.glob(os.path.join(basedir, '*.json'))
random.shuffle(fns)
for fn in fns[:int(len(fns) * 0.9)]:
    shutil.move(fn, os.path.join(train_basedir, os.path.basename(fn)))
for fn in fns[int(len(fns) * 0.9):]:
    shutil.move(fn, os.path.join(test_basedir, os.path.basename(fn)))

fns = fns = glob.glob(os.path.join(train_basedir, '*.json'))
jpgs = glob.glob(os.path.join(train_basedir, '*'))
for fn in tqdm.tqdm(fns[:]):
    with open(fn) as f:
        json_data = json.load(f)
    shapes = json_data['shapes']
    img_path = json_data['imagePath']
    if os.path.join(train_basedir,img_path) not in jpgs:
        img_data = json_data['imageData']
        img_data = img_b64_to_arr(img_data)
    else:
        img_data = cv2.imread(os.path.join(train_basedir,img_path))
    mask = np.zeros(img_data.shape[:2])
    h,w = img_data.shape[:2]
    shapes = json_data['shapes']
    poly_objs = [dict(points=el['points'],label=el['label'],shapetype=el['shape_type']) for el in shapes]
    for poly in poly_objs:
        try:
            if poly['label'] == 'shangbianjie':
                points = np.array(poly['points']).astype(int)
                cv2.polylines(mask, np.array([points]).astype(np.int32), False, 1, 4)
        except Exception:
            traceback.print_exc()
            print(traceback.format_exc())
    for i in range(int(w//SIZE_W)):
        img_crop = img_data[0:SIZE_H,i*SIZE_W:(i+1)*SIZE_W]
        mask_crop = mask[0:SIZE_H,i*SIZE_W:(i+1)*SIZE_W]
        tar_fn = os.path.basename(fn)[:-4] + '_shang_{}'.format(''.join(random.sample(string.ascii_letters + string.digits, 3)))
        tar_fn = os.path.join(train_targetdir, tar_fn)
        cv2.imwrite(tar_fn+'.jpg', img_crop)
        cv2.imwrite(tar_fn+'.png', mask_crop)
    for i in range(int(w//SIZE_W)):
        img_crop = img_data[img_data.shape[0]-SIZE_H:img_data.shape[0],i*SIZE_W:(i+1)*SIZE_W]
        mask_crop = mask[img_data.shape[0]-SIZE_H:img_data.shape[0],i*SIZE_W:(i+1)*SIZE_W]
        tar_fn = os.path.basename(fn)[:-4] + '_xia_{}'.format(''.join(random.sample(string.ascii_letters + string.digits, 3)))
        tar_fn = os.path.join(train_targetdir, tar_fn)
        cv2.imwrite(tar_fn+'.jpg', img_crop)
        cv2.imwrite(tar_fn+'.png', mask_crop)
        

fns = fns = glob.glob(os.path.join(test_basedir, '*.json'))
jpgs = glob.glob(os.path.join(test_basedir, '*'))

for fn in tqdm.tqdm(fns[:]):
    with open(fn) as f:
        json_data = json.load(f)
    shapes = json_data['shapes']
    img_path = json_data['imagePath']
    if os.path.join(test_basedir,img_path) not in jpgs:
        img_data = json_data['imageData']
        img_data = img_b64_to_arr(img_data)
    else:
        img_data = cv2.imread(os.path.join(test_basedir,img_path))

    mask = np.zeros(img_data.shape[:2])
    h,w = img_data.shape[:2]
    shapes = json_data['shapes']
    poly_objs = [dict(points=el['points'],label=el['label'],shapetype=el['shape_type']) for el in shapes]

    for poly in poly_objs:
        try:
            if poly['label'] == 'shangbianjie':
                points = np.array(poly['points']).astype(int)
                cv2.polylines(mask, np.array([points]).astype(np.int32), False, 1, 4)
        except Exception:
            print(fn, points)
            traceback.print_exc()
            print(traceback.format_exc())
    for i in range(int(w//SIZE_W)):
        img_crop = img_data[0:SIZE_H,i*SIZE_W:(i+1)*SIZE_W]
        mask_crop = mask[0:SIZE_H,i*SIZE_W:(i+1)*SIZE_W]
        tar_fn = os.path.basename(fn)[:-4] + '_shang_{}'.format(''.join(random.sample(string.ascii_letters + string.digits, 3)))
        tar_fn = os.path.join(test_targetdir, tar_fn)
        cv2.imwrite(tar_fn+'.jpg', img_crop)
        cv2.imwrite(tar_fn+'.png', mask_crop)
    for i in range(int(w//SIZE_W)):
        img_crop = img_data[img_data.shape[0]-SIZE_H:img_data.shape[0],i*SIZE_W:(i+1)*SIZE_W]
        mask_crop = mask[img_data.shape[0]-SIZE_H:img_data.shape[0],i*SIZE_W:(i+1)*SIZE_W]
        tar_fn = os.path.basename(fn)[:-4] + '_xia_{}'.format(''.join(random.sample(string.ascii_letters + string.digits, 3)))
        tar_fn = os.path.join(test_targetdir, tar_fn)
        cv2.imwrite(tar_fn+'.jpg', img_crop)
        cv2.imwrite(tar_fn+'.png', mask_crop)
        
