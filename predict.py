import os
os.environ['CUDA_VISIBLE_DEVICES'] = '3'
import cv2
import keras
import numpy as np
import matplotlib.pyplot as plt
import random
import glob
import albumentations as A
import tensorflow as tf

SIZE_W, SIZE_H, SIZE_H_1, SIZE_H_3= 640, 320, 1370, 1432

# y_train_dir = x_train_dir = [
#         # '../datasets/512x512/train'
#         '/mnt/nfs/zhangqiu/2stage/code/data/0-60/mask/train_mask'
#         ]
y_valid_dir = x_valid_dir = [
        '/mnt/nfs/public/xin-yuan/raw-data/20210105/3/crop/'
        ]

class Dataloder(keras.utils.Sequence):
    """Load data from dataset and form batches

    Args:
        dataset: instance of Dataset class for image loading and preprocessing.
        batch_size: Integet number of images in batch.
        shuffle: Boolean, if `True` shuffle image indexes each epoch.
    """

    def __init__(self, dataset, batch_size=1, shuffle=False):
        self.dataset = dataset
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.indexes = np.arange(len(dataset))

        self.on_epoch_end()

    def __getitem__(self, i):

        # collect batch data
        start = i * self.batch_size
        stop = (i + 1) * self.batch_size
        data = []
        for j in range(start, stop):
            data.append(self.dataset[j])

        # transpose list of lists
        batch = [np.stack(samples, axis=0) for samples in zip(*data)]
        # data = [[1,4,7,10,15],
        # [2,5,8,12,19], 
        # [3,6,9,16,22],
        # [10,13,14,17,24], 
        # [18,21,23,26,30]]
        # data = list(zip(*data))# 每列的数据组成一个元组 , 再一起组成一个列表
        # data = list(zip(*data))#内容又变成了原来的 , 元素从小列表变成小元组
        return batch

    def __len__(self):
        """Denotes the number of batches per epoch"""
        return len(self.indexes) // self.batch_size

    def on_epoch_end(self):
        """Callback function to shuffle indexes each epoch"""
        if self.shuffle:
            self.indexes = np.random.permutation(self.indexes)


class Dataset:
    # CLASSES = ['bg', 'liewen']
    CLASSES = ['bg', 'shangbianjie']


    def __init__(
            self,
            images_dir,
            masks_dir,
            classes=None,
            augmentation=None,
            preprocessing=None,
    ):
        self.ids = []
        for d in images_dir:
            self.ids += glob.glob(os.path.join(d, '*.jpg'))
            self.ids += glob.glob(os.path.join(d, '*.bmp'))
        self.ids_gt_vis = []
        # for d in masks_visualized_dir:
        #     self.ids_gt_vis += glob.glob(os.path.join(d,'*.jpg'))
        self.images_fps = self.ids
        self.masks_fps = [image_id.replace('.jpg', '.png') for image_id in self.ids]

        self.class_values = [self.CLASSES.index(cls.lower()) for cls in classes]

        self.augmentation = augmentation
        self.preprocessing = preprocessing
    def __getitem__(self, i):

        # read data
        image = cv2.imread(self.images_fps[i])
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        mask = cv2.imread(self.masks_fps[i], 0)

        # extract certain classes from mask (e.g. cars)
        masks = [(mask == v) for v in self.class_values]
        mask = np.stack(masks, axis=-1).astype('float')

        # add background if mask is not binary
        if mask.shape[-1] != 1:
            background = 1 - mask.sum(axis=-1, keepdims=True)
            mask = np.concatenate((mask, background), axis=-1)

        origin_image = image.copy()
        # apply augmentations
        if self.augmentation:
            sample = self.augmentation(image=image, mask=mask)
            image, mask = sample['image'], sample['mask']

        # apply preprocessing
        if self.preprocessing:
            sample = self.preprocessing(image=image)
            image = sample['image']

        return image, mask, origin_image

    def __len__(self):
        return len(self.ids)

# define heavy augmentations
def get_training_augmentation():
    train_transform = [

        A.HorizontalFlip(p=0.5),

        A.ShiftScaleRotate(scale_limit=0.5, rotate_limit=0, shift_limit=0.1, p=0.3, border_mode=0),

        A.PadIfNeeded(min_height=SIZE_H, min_width=SIZE_W, always_apply=True, border_mode=0),
        A.RandomCrop(height=SIZE_H, width=SIZE_W, always_apply=True),

        A.IAAPerspective(p=0.2),

    ]
    return A.Compose(train_transform)

def get_validation_augmentation():
    """Add paddings to make image shape divisible by 32"""
    test_transform = [
        # A.PadIfNeeded(2048, 2592)
        # A.PadIfNeeded(min_height=SIZE_H, min_width=SIZE_W)
    ]
    return A.Compose(test_transform)

def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform

    Args:
        preprocessing_fn (callbale): data normalization function
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose

    """

    _transform = [
        A.Lambda(image=preprocessing_fn),
    ]
    return A.Compose(_transform)

import segmentation_models as sm

BACKBONE = 'efficientnetb0'
BATCH_SIZE = 8
CLASSES = ['shangbianjie']
LR = 0.001
EPOCHS = 30

preprocess_input = sm.get_preprocessing(BACKBONE)

n_classes = 1 if len(CLASSES) == 1 else (len(CLASSES) + 1)  # case for binary and multiclass segmentation
activation = 'sigmoid' if n_classes == 1 else 'softmax'

#create model
model = sm.Unet(BACKBONE, classes=n_classes, activation=activation, decoder_block_type='transpose')

optim = keras.optimizers.Adam(LR)

# Segmentation models losses can be combined together by '+' and scaled by integer or float factor
# set class weights for dice_loss (car: 1.; pedestrian: 2.; background: 0.5;)

# dice_loss = sm.losses.DiceLoss(class_weights=np.array([1, 0.5,0.5]))
dice_loss = sm.losses.DiceLoss(class_weights=np.array([1, 0.5]))

focal_loss = sm.losses.BinaryFocalLoss() if n_classes == 1 else sm.losses.CategoricalFocalLoss()
total_loss = dice_loss + (1 * focal_loss)

# actulally total_loss can be imported directly from library, above example just show you how to manipulate with losses
# total_loss = sm.losses.binary_focal_dice_loss # or sm.losses.categorical_focal_dice_loss

# metrics = [sm.metrics.IOUScore(threshold=0.5, class_weights=np.array([1, 0.5,0.5])), sm.metrics.FScore(threshold=0.5, class_weights=np.array([1, 0.5,0.5]))]
metrics = [sm.metrics.IOUScore(threshold=0.5, class_weights=np.array([1, 0.5])), sm.metrics.FScore(threshold=0.5, class_weights=np.array([1,0.5]))]

# compile keras model with defined optimozer, loss and metrics
model.compile(optim, total_loss, metrics)

# model.load_weights('./saved_model/0801_640x320_v1/best.h5')
# model.load_weights('./saved_model/0808_640x320_v1/best.h5')
model.load_weights('/mnt/nfs/zhangqiu/xinyuan/saved_model/id3/best.h5')

# Dataset for validation images
valid_dataset = Dataset(
    x_valid_dir,
    y_valid_dir,
    classes=CLASSES,
    augmentation=get_validation_augmentation(),
    preprocessing=get_preprocessing(preprocess_input),
)

valid_dataloader = Dataloder(valid_dataset, batch_size=1, shuffle=False)

# out = '/mnt/nfs/zhangqiu/2stage/code/data/prediction/20201230-3-1'
# if not os.path.exists(out):
#     os.mkdir(out)
# img_da_path = '/mnt/nfs/zhangqiu/2stage/code/data/20201230-1-1'


out = '/mnt/nfs/zhangqiu/xinyuan/predict/20210104-0-0'

if not os.path.exists(out):
    os.mkdir(out)

l_shang = []
l_xia = []

for i in np.arange(len(valid_dataset)):
    image, gt_mask, origin_image  = valid_dataset[i]
    image = np.expand_dims(image, axis=0)
    m = np.zeros_like(origin_image)
    m[:,:] = [0,0,255]
    pr_mask_shang = model.predict(image)[0]
    pr_mask_shang = pr_mask_shang[:,:,0]
    merge = m * pr_mask_shang[:,:,np.newaxis]
    import numpy as np
    result = np.hstack([merge,origin_image])
    cv2.imwrite(os.path.join(out,os.path.basename(valid_dataset.images_fps[i])), result.astype(int))

    _,RedThresh = cv2.threshold(merge,1,255,cv2.THRESH_BINARY)
    h, w = RedThresh.shape[:2]
    mask = np.zeros_like(RedThresh)
    sum_y = 0
    count = 0
    for i_h in range(h):
        for j_w in range(260,460):
            try:
                if RedThresh[i_h,j_w,2] == 255:
                    sum_y += i_h
                    count += 1
            except Exception:
                import ipdb; ipdb.set_trace()
    if i % 2 == 0:
        if count != 0:
            l_shang.append(sum_y //count)
        else:
            l_shang.append(l_shang[-1])
    else:
        if count != 0:
            l_xia.append(SIZE_H_1 - SIZE_H + sum_y//count)
        else:
            l_xia.append(l_xia[-1])

print('l_shang: {}'.format(l_shang))
print('l_xia: {}'.format(l_xia))



