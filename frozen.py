import numpy as np
import matplotlib.pyplot as plt
import glob
from keras import backend as K
import keras
K.set_image_data_format('channels_first')
K.set_learning_phase(0)
import sys

import tensorflow as tf
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '0'
from tensorflow.python.framework import graph_io
import segmentation_models as sm
# sm.set_framework('tf.keras')


def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    """
    Freezes the state of a session into a pruned computation graph.

    Creates a new computation graph where variable nodes are replaced by
    constants taking their current value in the session. The new graph will be
    pruned so subgraphs that are not necessary to compute the requested
    outputs are removed.
    @param session The TensorFlow session to be frozen.
    @param keep_var_names A list of variable names that should not be frozen,
                          or None to freeze all the variables in the graph.
    @param output_names Names of the relevant graph outputs.
    @param clear_devices Remove the device directives from the graph for better portability.
    @return The frozen graph definition.
    """
    from tensorflow.python.framework.graph_util import convert_variables_to_constants
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = convert_variables_to_constants(session, input_graph_def,
                                                      output_names, freeze_var_names)
        return frozen_graph

def frozen_to_pb(model,pbname = './my_model.pb'):
    #model.summary()
#     model.save("lastFrozenModelV2.h5")
    frozen_graph = freeze_session(K.get_session(),
                                  output_names=[out.op.name for out in model.outputs])
    frozen_graph = tf.graph_util.remove_training_nodes(frozen_graph)
    tf.train.write_graph(frozen_graph, os.path.split(pbname)[0], os.path.split(pbname)[1], as_text=False)
    output_names=[out.op.name for out in model.outputs]
    output_names = output_names or []
    output_names += [v.op.name for v in tf.global_variables()]
    #print("outputName: ", output_names)
    print('output_tensor: ', model.outputs)
    print('input_tensor: ', model.inputs)

# BACKBONE = 'efficientnetb3'
# BATCH_SIZE = 8
# CLASSES = ['yuan']
# LR = 0.001

BACKBONE = 'efficientnetb0' 
BATCH_SIZE = 4
CLASSES = ['liewen']
LR = 0.001
EPOCHS = 200

preprocess_input = sm.get_preprocessing(BACKBONE)

n_classes = 1 if len(CLASSES) == 1 else (len(CLASSES) + 1)  # case for binary and multiclass segmentation
activation = 'sigmoid' if n_classes == 1 else 'softmax'

#create model
model = sm.Unet(BACKBONE, classes=n_classes, activation=activation, input_shape=(3,None,None), decoder_block_type='transpose', encoder_weights=None)
# model = sm.Unet(BACKBONE, classes=n_classes, activation=activation, decoder_block_type='transpose')


optim = keras.optimizers.Adam(LR)

# Segmentation models losses can be combined together by '+' and scaled by integer or float factor
# set class weights for dice_loss (car: 1.; pedestrian: 2.; background: 0.5;)
dice_loss = sm.losses.DiceLoss(class_weights=np.array([1, 0.5]))
focal_loss = sm.losses.BinaryFocalLoss() if n_classes == 1 else sm.losses.CategoricalFocalLoss()
total_loss = dice_loss + (1 * focal_loss)

# actulally total_loss can be imported directly from library, above example just show you how to manipulate with losses
# total_loss = sm.losses.binary_focal_dice_loss # or sm.losses.categorical_focal_dice_loss

metrics = [sm.metrics.IOUScore(threshold=0.5, class_weights=np.array([1, 0.5])), sm.metrics.FScore(threshold=0.5, class_weights=np.array([1, 0.5]))]

# compile keras model with defined optimozer, loss and metrics
model.compile(optim, total_loss, metrics)
model_path = sys.argv[1]
model.load_weights(model_path, by_name=True)

# for i in range(len(model.layers)):
    # print(model.get_layer(index=i).output)

new_model = keras.Model(input=model.input, output=model.get_layer('sigmoid').output)

pb_path = sys.argv[2]
frozen_to_pb(new_model, pb_path)
