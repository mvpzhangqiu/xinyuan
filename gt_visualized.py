import cv2
import os
import numpy as np

def read_path(file_pathname):
    #遍历该目录下的所有图片文件
    for filename in os.listdir(file_pathname):
        if filename[-3:] == 'png':
            mask = cv2.imread(file_pathname+'/'+filename)
            mask_vis = np.zeros((mask.shape[0], mask.shape[1], 3), np.uint8)
            # print(mask[:,:, 0].type)
            print('**********************')
            mask_vis[mask[:,:, 0]==1] = [0, 0, 255] # red
            # b = mask[:,:,0]==1
            # print(b)
            # mask_vis[mask[:,:,0]==1] = 255# red

            #path为输入训练crop图像地址
            path = '/mnt/nfs/public/xin-yuan/anno-data/20210104-0-0/mask/train_mask/'+filename[:-3]+'jpg'
            origin = cv2.imread(path) 
            merge = np.hstack([origin,mask_vis])
            cv2.imwrite('/mnt/nfs/zhangqiu/xinyuan/predict/gt/'+filename[:-3]+'jpg',merge)   


read_path("/mnt/nfs/public/xin-yuan/anno-data/20210104-0-0/mask/train_mask")



