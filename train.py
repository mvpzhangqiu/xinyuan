import os
os.environ['CUDA_VISIBLE_DEVICES'] = '0,1'

from cv2 import cv2
import keras
import numpy as np
import matplotlib.pyplot as plt
import random
import glob
import albumentations as A

import tensorflow as tf

SIZE_W, SIZE_H = 640, 320



y_train_dir = x_train_dir = [
    '/mnt/nfs/public/xin-yuan/anno-data/20210104-0-0/mask/train_mask/',
    '/mnt/nfs/public/xin-yuan/anno-data/20210106-0-0/mask/train_mask/'
        ]

        
y_valid_dir = x_valid_dir = [
    '/mnt/nfs/public/xin-yuan/anno-data/20210104-0-0/mask/test_mask/' ,
    '/mnt/nfs/public/xin-yuan/anno-data/20210106-0-0/mask/test_mask/'
        ]
model_name = 'id3'
model_savedir = os.path.join('/mnt/nfs/zhangqiu/xinyuan/saved_model', model_name)
if not os.path.exists(model_savedir):
    os.makedirs(model_savedir)


class Dataloder(keras.utils.Sequence):
    """Load data from dataset and form batches

    Args:
        dataset: instance of Dataset class for image loading and preprocessing.
        batch_size: Integet number of images in batch.
        shuffle: Boolean, if `True` shuffle image indexes each epoch.
    """

    def __init__(self, dataset, batch_size=1, shuffle=False):
        self.dataset = dataset
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.indexes = np.arange(len(dataset))

        self.on_epoch_end()

    def __getitem__(self, i):

        # collect batch data
        start = i * self.batch_size
        stop = (i + 1) * self.batch_size
        data = []
        for j in range(start, stop):
            data.append(self.dataset[j])

        # transpose list of lists
        batch = [np.stack(samples, axis=0) for samples in zip(*data)]

        return batch

    def __len__(self):
        """Denotes the number of batches per epoch"""
        return len(self.indexes) // self.batch_size

    def on_epoch_end(self):
        """Callback function to shuffle indexes each epoch"""
        if self.shuffle:
            self.indexes = np.random.permutation(self.indexes)


class Dataset:
    # CLASSES = ['bg', 'liewen']
    CLASSES = ['bg','shangbianjie']
    def __init__(
            self,
            images_dir,
            masks_dir,
            classes=None,
            augmentation=None,
            preprocessing=None,
    ):
        self.ids = []
        for d in images_dir:
            print(d, len(glob.glob(os.path.join(d, '*.jpg'))))
            self.ids += glob.glob(os.path.join(d, '*.jpg'))

        self.images_fps = self.ids
        self.masks_fps = [image_id.replace('.jpg', '.png') for image_id in self.ids]

        self.class_values = [self.CLASSES.index(cls.lower()) for cls in classes]

        self.augmentation = augmentation
        self.preprocessing = preprocessing
    def __getitem__(self, i):
        # read data
        image = cv2.imread(self.images_fps[i])
        #cv2.imread() 默认以BGR格式读取图片
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        mask = cv2.imread(self.masks_fps[i], 0)

        # extract certain classes from mask (e.g. cars)
        masks = [(mask == v) for v in self.class_values]
        mask = np.stack(masks, axis=-1).astype('float')

        # add background if mask is not binary
        if mask.shape[-1] != 1:
            background = 1 - mask.sum(axis=-1, keepdims=True)
            mask = np.concatenate((mask, background), axis=-1)

        # apply augmentations
        if self.augmentation:
            sample = self.augmentation(image=image, mask=mask)
            # cv2.imwrite('/mnt/nfs/zhangqiu/2stage/code/output_test/origin/{}.png'.format(i),image)
            image, mask = sample['image'], sample['mask']
            # cv2.imwrite('/mnt/nfs/zhangqiu/2stage/code/output_test/augmentation/{}.png'.format(i),image)

        # apply preprocessing
        if self.preprocessing:
            sample = self.preprocessing(image=image)
            image = sample['image']

        return image, mask

    def __len__(self):
        return len(self.ids)

# define heavy augmentations
def get_training_augmentation():
    train_transform = [

        A.HorizontalFlip(p=0.5),

        A.ShiftScaleRotate(scale_limit=0.5, rotate_limit=0, shift_limit=0.1, p=0.3, border_mode=0),

        A.PadIfNeeded(min_height=SIZE_H, min_width=SIZE_W, always_apply=True, border_mode=0),
        A.RandomCrop(height=SIZE_H, width=SIZE_W, always_apply=True),

        A.IAAPerspective(p=0.2),

    ]
    # train_transform = [
    #     A.RandomRotate90(),
    #     A.Flip(),
    #     # A.Transpose(),
    #     A.OneOf([
    #         A.IAAAdditiveGaussianNoise(),
    #         A.GaussNoise(),
    #     ], p=0.2),
    #     A.OneOf([
    #         A.MotionBlur(p=0.2),
    #         A.MedianBlur(blur_limit=3, p=0.1),
    #         A.Blur(blur_limit=3, p=0.1),
    #     ], p=0.2),
    #     A.ShiftScaleRotate(shift_limit=0.0625, scale_limit=0.2, rotate_limit=45, p=0.2),
    #     A.OneOf([
    #         A.OpticalDistortion(p=0.3),
    #         A.GridDistortion(p=0.1),
    #         A.IAAPiecewiseAffine(p=0.3),
    #     ], p=0.2),
    #     A.OneOf([
    #         A.CLAHE(clip_limit=2),
    #         A.IAASharpen(),
    #         A.IAAEmboss(),
    #         A.RandomBrightnessContrast(),
    #     ], p=0.3),
    #     A.HueSaturationValue(p=0.3),
    # ]
    return A.Compose(train_transform)

def get_validation_augmentation():
    """Add paddings to make image shape divisible by 32"""
    test_transform = [
#         A.PadIfNeeded(2048, 2592)
        A.PadIfNeeded(min_height=SIZE_H, min_width=SIZE_W)
    ]
    return A.Compose(test_transform)

def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform

    Args:
        preprocessing_fn (callbale): data normalization function
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose

    """

    _transform = [
        A.Lambda(image=preprocessing_fn),
    ]
    return A.Compose(_transform)

import segmentation_models as sm

BACKBONE = 'efficientnetb0' 
BATCH_SIZE = 4
CLASSES = ['shangbianjie']
LR = 0.001
EPOCHS = 200



preprocess_input = sm.get_preprocessing(BACKBONE)

n_classes = 1 if len(CLASSES) == 1 else (len(CLASSES) + 1)  # case for binary and multiclass segmentation
activation = 'sigmoid' if n_classes == 1 else 'softmax'

#create model
model = sm.Unet(BACKBONE, classes=n_classes, activation=activation, decoder_block_type='transpose')

optim = keras.optimizers.Adam(LR)

# Segmentation models losses can be combined together by '+' and scaled by integer or float factor
# set class weights for dice_loss (car: 1.; pedestrian: 2.; background: 0.5;)

dice_loss = sm.losses.DiceLoss(class_weights=np.array([1, 0.5]))
# dice_loss = sm.losses.DiceLoss(class_weights=np.array([1, 0.5,0.5]))

focal_loss = sm.losses.BinaryFocalLoss() if n_classes == 1 else sm.losses.CategoricalFocalLoss()
total_loss = dice_loss + (1 * focal_loss)

# actulally total_loss can be imported directly from library, above example just show you how to manipulate with losses
# total_loss = sm.losses.binary_focal_dice_loss # or sm.losses.categorical_focal_dice_loss

metrics = [sm.metrics.IOUScore(threshold=0.5, class_weights=np.array([1, 0.5])), sm.metrics.FScore(threshold=0.5, class_weights=np.array([1, 0.5]))]
# metrics = [sm.metrics.IOUScore(threshold=0.5, class_weights=np.array([1, 0.5,0.5])), sm.metrics.FScore(threshold=0.5, class_weights=np.array([1, 0.5,0.5]))]


# compile keras model with defined optimozer, loss and metrics
model.compile(optim, total_loss, metrics)

# model.load_weights('./finetune_0326_v8/best.h5')

train_dataset = Dataset(
    x_train_dir,
    y_train_dir,
    classes=CLASSES,
    augmentation=get_training_augmentation(),
    preprocessing=get_preprocessing(preprocess_input),
)

# Dataset for validation images
valid_dataset = Dataset(
    x_valid_dir,
    y_valid_dir,
    classes=CLASSES,
    augmentation=get_validation_augmentation(),
    preprocessing=get_preprocessing(preprocess_input),
)

train_dataloader = Dataloder(train_dataset, batch_size=BATCH_SIZE, shuffle=True)
valid_dataloader = Dataloder(valid_dataset, batch_size=1, shuffle=False)

# check shapes for errors
assert train_dataloader[0][0].shape == (BATCH_SIZE, SIZE_H, SIZE_W, 3)
assert train_dataloader[0][1].shape == (BATCH_SIZE, SIZE_H, SIZE_W, n_classes)

# define callbacks for learning rate scheduling and best checkpoints saving
callbacks = [
    keras.callbacks.ModelCheckpoint(os.path.join(model_savedir, 'best.h5'), save_weights_only=False, save_best_only=False, period=1,mode='max', monitor='val_iou_score'),
    keras.callbacks.ReduceLROnPlateau(),
]

history = model.fit_generator(
    train_dataloader,
    steps_per_epoch=len(train_dataloader),
    epochs=EPOCHS,
    callbacks=callbacks,
    validation_data=valid_dataloader,
    validation_steps=len(valid_dataloader),
)
