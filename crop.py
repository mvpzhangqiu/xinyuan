import os
os.environ['CUDA_VISIBLE_DEVICES'] = '3'
import cv2
import keras
import numpy as np
import matplotlib.pyplot as plt
import random
import glob
import albumentations as A
import tensorflow as tf

SIZE_H = 320
SIZE_W = 640

paths = [
    '/mnt/nfs/public/xin-yuan/raw-data/20210105/3/',
]

for path in paths:
    fns = glob.glob(os.path.join(path,'*.bmp'))
    for fn in fns:
        out = os.path.join(path,'crop')
        if not os.path.exists(out):
            os.mkdir(out)
        img = cv2.imread(fn)
        img_shang = img[0:SIZE_H,SIZE_W:SIZE_W * 2,:]
        img_xia = img[img.shape[0] - SIZE_H:img.shape[0],SIZE_W:SIZE_W * 2,:]
        cv2.imwrite(os.path.join(out,os.path.basename(fn).replace('.bmp','.shang.jpg')),img_shang)
        cv2.imwrite(os.path.join(out,os.path.basename(fn).replace('.bmp','.xia.jpg')),img_xia)
        
