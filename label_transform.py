import os
import cv2
import json

def change_label(file_pathname):
    for file in os.listdir(file_pathname):
        if file[-4:] == 'json':
            path = os.path.join(file_pathname,file)
            with open(path) as fp:
                json_data = json.load(fp)
            shapes = json_data['shapes']
            for d in shapes:
                if d['label'] == 'xiabianjie':
                    d['label'] = 'shangbianjie'
            json_data['shapes'] = shapes
            json.dump(json_data,open(path,'w'))
            
change_label('/mnt/nfs/public/xin-yuan/anno-data/20210104-0-0/test')

def change_imgpath(file_pathname):
    for file in os.listdir(file_pathname):
        if file[-4:] == 'json':
            path = os.path.join(file_pathname,file)
            with open(path) as fp:
                json_data = json.load(fp)
            json_data['imagePath'] = file.split('.')[0]+'.bmp' 
            json.dump(json_data,open(path,'w'))
            
# change_imgpath('/mnt/nfs/zhangqiu/2stage/code/data/20201127-0-0')